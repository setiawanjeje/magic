module.exports = {
  'extends': [ 
    'eslint:recommended',
    'airbnb-typescript', 
  ],
  parser: '@typescript-eslint/parser',
  root: true,
  env: {
    browser: true,
    es2021: true, 
    node: true, 
  },    
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 'latest',
    sourceType: 'module',
    project: './tsconfig.json',
  },
  plugins: [
    'react',
    '@typescript-eslint',
    'import', 
  ],
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
      },
    },

  },
};
 