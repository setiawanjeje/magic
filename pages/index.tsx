import type { NextPage } from 'next';
import Head from 'next/head';
import { useEffect, useState } from 'react';
import axios from 'axios';

import Pagination from '@mui/material/Pagination';
import UserTable from '../src/components/UserTable';
import FilterBox from '../src/components/FilterBox';

import styles from '../src/styles/Home.module.css';
import { Person } from '../src/types';
import { SelectChangeEvent } from '@mui/material';

const Home: NextPage = () => {
  const [data, setData] = useState<Person[] | undefined>(undefined);
  const [isLoading, setIsLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [inputVal, setInputVal] = useState<string>('');
  const [searchKeyword, setSearchKeyword] = useState<string>('');
  const [genderFilterVal, setGenderFilterVal] = useState<string>('');
  const [sortOrder, setSortOrder] = useState<'asc' | 'desc' | undefined>(undefined);
  const [sortBy, setSortBy] = useState<string>('');

  const fetch = () => {
    setIsLoading(true);

    axios.get(`https://randomuser.me/api/?page=${page}&results=10&inc=name,email,registered,login,gender${searchKeyword ? `&keyword=${searchKeyword}` : ''}${genderFilterVal ? `&gender=${genderFilterVal}` : ''}${sortBy ? `&sortBy=${sortBy}` : ''}${sortOrder ? `&sortOrder=${sortOrder}` : ''}`)
      .then((res) => {
        setData(res.data.results);
      }).catch((err) => {
        console.error(err);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    fetch();
  }, [searchKeyword, page, genderFilterVal, sortBy, sortOrder]);

  const handleChangePagination = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
  };

  const handleResetFilter = () => {
    setInputVal('');
    setSearchKeyword('');
    setGenderFilterVal('');
    setSortBy('');
    setSortOrder(undefined);
    setPage(1);
  };

  const handleSort = (property: string) => {
    const isAsc = sortBy === property && sortOrder === 'asc';
    setSortOrder(isAsc ? 'desc' : 'asc');
    setSortBy(property);
  };

  const handleChangeGenderFilter = (e:SelectChangeEvent) => {
    setGenderFilterVal(e.target.value);
    setPage(1);
  };

  const handleSearch = () => {
    setSearchKeyword(inputVal);
  };

  return (
    <div>
      <Head>
        <title>User list</title>
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          User list
        </h1> 

        <FilterBox 
          handleChangeGenderFilter={handleChangeGenderFilter}
          handleResetFilter={handleResetFilter}
          inputVal={inputVal}
          setInputVal={setInputVal}
          handleSearch={handleSearch}
          genderFilterVal={genderFilterVal}
        />

        <UserTable
          data={data}
          handleSort={handleSort}
          isLoading={isLoading}
          sortBy={sortBy}
          sortOrder={sortOrder}
        />

        <div style={{ marginTop: 16 }}>
          <Pagination count={10} page={page} onChange={handleChangePagination} />
        </div>
      </main>
    </div>
  );
};

export default Home;
