import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import TableSortLabel from '@mui/material/TableSortLabel';
import format from 'date-fns/format';
import { Person } from '../types';

type UserTableProps = {
  sortBy: string,
  sortOrder: 'asc' | 'desc' | undefined,
  handleSort: (property: string) => void,
  isLoading: boolean,
  data: Person[] | undefined
};

const UserTable = (props: UserTableProps) => {
  const {
    data, 
    handleSort,
    isLoading,
    sortBy,
    sortOrder,
  } = props;

  return ( 
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Username</TableCell>
            <TableCell>
              <TableSortLabel
                active={sortBy === 'name'}
                direction={sortBy === 'name' ? sortOrder : 'asc'}
                onClick={() => {handleSort('name');}}
              >
                Name
              </TableSortLabel>
            </TableCell>
            <TableCell>
              <TableSortLabel
                active={sortBy === 'email'}
                direction={sortBy === 'email' ? sortOrder : 'asc'}
                onClick={() => {handleSort('email');}}
              >
                Email
              </TableSortLabel>
            </TableCell>
            <TableCell>
              <TableSortLabel
                active={sortBy === 'gender'}
                direction={sortBy === 'gender' ? sortOrder : 'asc'}
                onClick={() => {handleSort('gender');}}
              >
                Gender
              </TableSortLabel>
            </TableCell>
            <TableCell>Registered Date</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {isLoading ? <TableRow><TableCell colSpan={5} align="center">Loading...</TableCell></TableRow> : (
            <>
              {data?.map((person) => (
                <TableRow
                  key={person.name.first}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {person.login.username}
                  </TableCell>
                  <TableCell>{`${person.name.first} ${person.name.last}`}</TableCell>
                  <TableCell>{person.email}</TableCell>
                  <TableCell>{person.gender}</TableCell>
                  <TableCell>{format(new Date(person.registered.date), 'dd-MM-yyyy HH:mm')}</TableCell>
                </TableRow>
              ))}
            </>
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default UserTable;