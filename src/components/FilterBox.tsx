import TextField from '@mui/material/TextField';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import Button from '@mui/material/Button';

type FilterBoxProps = {
  inputVal: string,
  setInputVal: (val: string)  => void,
  handleSearch: () => void,
  handleChangeGenderFilter: (e: SelectChangeEvent) => void,
  handleResetFilter: () => void,
  genderFilterVal: string,
};

const FilterBox = (props: FilterBoxProps) => {
  const {
    handleChangeGenderFilter,
    inputVal,
    setInputVal,
    handleSearch,
    handleResetFilter,
    genderFilterVal,
  } = props;

  return (
    <div style={{ marginBottom: 16, display: 'flex', gap: 16 }}>

      <div style={{ width: 200 }}>
        <FormControl fullWidth>
          <InputLabel id="select-gender-label">Gender</InputLabel>
          <Select
            labelId="select-gender-label"
            id="select-gender"
            value={genderFilterVal}
            label="Gender"
            onChange={handleChangeGenderFilter}
          >
            <MenuItem value={'female'}>female</MenuItem>
            <MenuItem value={'male'}>male</MenuItem>
          </Select>
        </FormControl>
      </div>


      <TextField id="standard-basic" label="Search" variant="standard" value={inputVal} onChange={(e) => setInputVal(e.target.value)} />
      
      <Button variant="outlined" onClick={handleSearch}>Search</Button>

      <Button onClick={handleResetFilter}>Reset</Button>
    </div>
  );
};

export default FilterBox;