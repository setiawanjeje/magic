export type Person = {
  name: {
    first: string,
    last: string,
  }
  gender: 'female' | 'male',
  login: {
    username: string
  },
  email: string,
  registered: {
    date: string,
    age: number,

  }
};
